json.array!(@trainings) do |training|
  json.extract! training, :id, :name, :picture, :notes
  json.url training_url(training, format: :json)
end
