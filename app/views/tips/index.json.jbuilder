json.array!(@tips) do |tip|
  json.extract! tip, :id, :name, :notes
  json.url tip_url(tip, format: :json)
end
