Bodybuilding::Application.routes.draw do
  root to: "home#index"

  get "home/index"

  resources :tips
  resources :trainings
  resources :foods
end
