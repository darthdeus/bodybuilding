class CreateTrainings < ActiveRecord::Migration
  def change
    create_table :trainings do |t|
      t.string :name
      t.string :picture
      t.string :notes

      t.timestamps
    end
  end
end
