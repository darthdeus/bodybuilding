class CreateFoods < ActiveRecord::Migration
  def change
    create_table :foods do |t|
      t.string :name
      t.string :category
      t.integer :energy
      t.integer :protein
      t.integer :sugar
      t.integer :fat
      t.string :notes

      t.timestamps
    end
  end
end
